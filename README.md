# Teste Frontend SEOX


## Considerações

1. O projeto foi desenvolvido sem Framework Frontend.
2. Utilizei o Swiper Slide para a construção dos carrosseis.
3. Nunca tinha utilizado o Gitlab pages, pois costumo usar mais o Github pages, desta forma não sei se foi tudo configurado corretamente. Acredito que sim 😁. Caso eu não tenha feito isso corretamente, também coloquei o teste nesse link https://francismar.dev

## Instruções para rodar local

1. Clonar o repositório
2. Executar o index.html 
3. Caso queira também dá pra rodar o Node para compilar algo do SASS.
4. Execute no terminal 

```sh
npm install
```

5. e depois o

```sh
 npm run gulp watch
```

