
var swiper = new Swiper(".carrosselVideos", {
    slidesPerView: 1.2,
    spaceBetween: 24,
    loop: true,
    navigation: {
        nextEl: ".sec--videos .nav-prev",
        prevEl: ".sec--videos .nav-next",
    },
    breakpoints: {
        768: {
            slidesPerView: 2.3,
            direction: "vertical",
            loop: false,
        },
        1200: {
            slidesPerView: 2.1,
            direction: "vertical",
            loop: false,
        },
        1400: {
            slidesPerView: 2,
            direction: "vertical",
            loop: false,
        }

    },
}); 

var swiper = new Swiper(".carrosselColunistas", {
    slidesPerView: "auto",
    spaceBetween: 0,
    loop: true,
    navigation: {
        nextEl: ".sec--colunistas .nav-next",
        prevEl: ".sec--colunistas .nav-prev",
    },
    breakpoints: {
        768: {
            slidesPerView: "auto",
            spaceBetween: 24,
            loop: true,
        },
    },
}); 


document.addEventListener('DOMContentLoaded', () => {

    document.querySelectorAll('.card--colunista').forEach(e => {
        e.onmouseover = () => {
            timeOut(e)
        }
       
    });

    function timeOut(e){
        setTimeout(() => {
            e.scrollIntoView({
                behavior: 'smooth',
                block: 'center',
                inline: 'end',
            })
        }, 300)
    }

    
});
  