var gulp        = require('gulp');
var sass        = require('gulp-sass');
sass.compiler   = require('sass-embedded');
var concat      = require('gulp-concat');
var rename      = require('gulp-rename');
var uglify      = require('gulp-uglify');


// js file paths
var componentsJsPath = 'assets/js/custom/*.js'; // component js files
var scriptsJsPath = 'assets/js'; //folder for final scripts.js/scripts.min.js files

// css file paths
var cssFolder = 'assets/css'; // folder for final style.css/style-fallback.css files
var scssFilesPath = 'assets/sass/**/*.scss'; // scss files to watch


gulp.task('sass', function() {
  return gulp.src(scssFilesPath)
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(rename('custom.min.css'))
  .pipe(gulp.dest(cssFolder))
});

gulp.task('scripts', function() {
  return gulp.src(componentsJsPath)
  .pipe(uglify())
  .pipe(rename('scripts.min.js'))
  .pipe(gulp.dest(scriptsJsPath))
});

gulp.task('watch', gulp.series(['sass', 'scripts'], function () {
  gulp.watch(scssFilesPath, gulp.series(['sass']));
  gulp.watch(componentsJsPath, gulp.series(['scripts']));
}));
